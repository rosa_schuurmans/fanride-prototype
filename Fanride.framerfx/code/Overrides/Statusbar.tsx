import { Override } from "framer"
import * as moment from "moment"

export function ShowTime(): Override {
    return {
        text: moment().format("HH:mm"),
        Time: moment().format("HH:mm"),
    }
}

export function ShowDay(): Override {
    return {
        text: moment().format("dddd, D MMMM"),
    }
}

export function ShowWeekday(): Override {
    return {
        text: moment().format("dddd"),
    }
}
