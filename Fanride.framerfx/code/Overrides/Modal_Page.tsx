import { Data, Override } from "framer"
import { closeModal } from "../Navigation/Modal"
const data = Data({
    currentPage: 0,
})

let timeout: any = false

export function PageWrapper(props): Override {
    return {
        currentPage: data.currentPage
    }
}

export function Trigger(props): Override {
    return {
        onTap() {
            closeModal();
            data.currentPage++
        },
    }
}
