import * as React from "react"
import { Stack, ControlType, addPropertyControls } from "framer"
import { Rides } from "../Content"
import { GoTo } from "./Splash_Page"
import { getStateVal} from "../State"
import { url } from "framer/resource"


const getBadge = val => {
  return val.length > 1 ? url(`../../design/assets/badge-${val}.png`) : false
}

const getAvatar = val => {
  return val.length > 1 ? url(`../../design/assets/user-${val}.png`) : false
}


export function PageWrapper(props): Override {
    const ride = Rides[getStateVal('openRide')];
    return {
        ...ride,
        userBadges: `${ride.user}'s Badges'`,
        userCar: `${ride.user}'s Car'`,
        Badge_1: getBadge(ride.Badge_1),
        Badge_2: getBadge(ride.Badge_2),
        Badge_3: getBadge(ride.Badge_3),
        Badge_4: getBadge(ride.Badge_4),
        User_1: getAvatar(ride.User_1),
        User_2: getAvatar(ride.User_2),
        User_3: getAvatar(ride.User_3),
        User_4: getAvatar(ride.User_4),
    }
}