import { Data, Override } from "framer"
import { closeModal } from "../Navigation/Modal"

const data = Data({
    duration: 100,
    currentPage: 0,
})

// 1: splash, 2: overview, 3:event, 4: ride

let timeout: any = false

export const GoTo = pageName => {
    console.log("go to", pageName)
    if (pageName === "ride") {
        data.currentPage = 3
    }
    if (pageName === "splash") {
        data.currentPage = 0
    }
    if (pageName === "overview") {
        data.currentPage = 1
    }
    if (pageName === "event") {
        data.currentPage = 2
    }
}

export function PageWrapper(props): Override {
    if (!timeout) {
        timeout = window.setTimeout(() => {
            data.currentPage = 1
        }, data.duration)
    }
    return {
        currentPage: data.currentPage,
    }
}

export function Trigger(props): Override {
    return {
        onTap() {
            closeModal()
            GoTo("event")
        },
    }
}

export function GoToOverview(props): Override {
    return {
        onTap() {
            GoTo("overview")
        },
    }
}
  export function GoBack(props): Override {
    return {
        onTap() {
          console.log('go back')
            data.currentPage = data.currentPage - 1;
        },
    }
}

export function GoToRide(props): Override {
    return {
        onTap() {
            GoTo("ride")
        },
    }
}

export function GoToEvent(props): Override {
    return {
        onTap() {
            closeModal()
            GoTo("event")
        },
    }
}
