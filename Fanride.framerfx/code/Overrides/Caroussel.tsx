import { Data, Override } from "framer"
import { closeModal } from "../Navigation/Modal"

const data = Data({
    currentPage: 0,
    length: 2
})


export function PageWrapper(props): Override {
  
    return {
        currentPage: data.currentPage,
        onChangePage(currentIndex, previousIndex, pageComponent) {
          data.currentPage = currentIndex
        },
        onTap() {
          console.log('tap')
          data.currentPage = data.currentPage
        }
    }
}

export function getActiveNumber(props): Override {
    return {
        text: (data.currentPage + 1) + ' / ' + (data.length + 1)
    }
}
