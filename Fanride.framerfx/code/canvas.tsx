// WARNING: this file is auto generated, any changes will be lost
import { createDesignComponent, CanvasStore } from "framer"
const canvas = CanvasStore.shared(); // CANVAS_DATA;

export const colors = Object.freeze({
    /** #FFFFFF */
    "base": "var(--token-81a67138-b228-4cd9-88f2-1a4b9b759822, rgb(255, 255, 255))",
})
§