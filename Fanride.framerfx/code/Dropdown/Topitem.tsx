import * as React from "react"
import { Frame, Stack } from "framer"
import { colors } from "../canvas"
import { Icon } from "../Utils/Icon"

// Open Preview (CMD + P)
// API Reference: https://www.framer.com/api

const textStyles = {
    background: "white",
    fontFamily: "Nunito Black",
    fontWeight: "regular",
    fontSize: 20,
    padding: "0 0",
    outline: "none",
    border: "none",
}
export function Topitem(props) {
    const item = {
        chosen: { color: colors.base },
        default: { color: colors.grey },
    }

    const iconState = {
        open: { rotate: -180 },
        closed: { rotate: 0 },
    }

    return (
        <Stack
            direction="horizontal"
            alignment="center"
            background="white"
            height="auto"
            onTap={() => props.getTap()}
            gap={0}
            distribution="center"
            width="100%"
            style={{ zIndex: 2 }}
        >
            <Frame
                height={50}
                width="auto"
                variants={item}
                initial="chosen"
                style={textStyles}
            >
                {props.text}
            </Frame>

            <Frame
                width="auto"
                height="auto"
                background="auto"
                variants={iconState}
                initial="closed"
                animate={props.openComponent ? "open" : "closed"}
            >
                <Icon
                    width={25}
                    stretch={true}
                    height={25}
                    icon="keyboard_arrow_down"
                    color={colors.base}
                />
            </Frame>
        </Stack>
    )
}

Topitem.defaultProps = {
    text: "label",
    openComponent: false,
}
