import * as React from "react"
import { ControlType } from "framer"
import styled, { injectGlobal } from "styled-components"

injectGlobal`
@font-face {
    font-family: "Polaris";
    font-weight: 500;
    src: local('Polaris Book'), 
         local('Polaris-Book'), 
         url(/code/fonts/Polaris-Book.woff) format("woff");
}
@font-face {
    font-family: "Polaris-Bold";
    font-weight: 700;
    src: local('Polaris Bold'), 
         local('Polaris-Bold'), 
         url(/code/fonts/Polaris-Bold.woff) format("woff");
}
@font-face {
    font-family: "PolarisCondensed-Bold";
    font-weight: 700;
    src: local('Polaris Condensed Bold'), 
         local('PolarisCondensed-Bold'),
         url(/code/fonts/PolarisCondensed-Bold.woff) format("woff");
}
`
interface MaterialIconProps {
    icon: string
    color: string
    stretch: boolean
    height: number
}

export class Icon extends React.Component<MaterialIconProps> {
    static defaultProps = {
        icon: "favorite",
        color: "#6200ee",
        stretch: false,
        width: 24,
        height: 24,
    }

    static propertyControls = {
        icon: { type: ControlType.String, title: "Icon name" },
        color: { type: ControlType.Color, title: "Color" },
        stretch: { type: ControlType.Boolean, title: "Stretch" },
    }

    render() {
        return (
            <StyledIconFrame
                height={this.props.height}
                stretch={this.props.stretch}
            >
                <i
                    className="material-icons mdc-button__icon"
                    aria-hidden="true"
                    style={{ color: this.props.color }}
                >
                    {this.props.icon}
                </i>
            </StyledIconFrame>
        )
    }
}

/*
 ** STYLED FRAME FOR WRAPPER COMPONENT
 ** This maintains centering the icon within the parent Frame on the canvas.
 */

interface iconProps {
    stretch: boolean
    height: number
}

const StyledIconFrame = styled<iconProps, any>("div")`
  display: flex;
  width: 100%;
  height: 100%;
  align-items: center;
  justify-content: center;

  i {
    font-size: ${props => (props.stretch ? props.height : 24)}px !important;
  }
`
