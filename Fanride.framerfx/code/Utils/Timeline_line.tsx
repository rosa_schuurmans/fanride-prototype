import * as React from "react"
import { Frame, useCycle } from "framer"

// Open Preview (CMD + P)
// API Reference: https://www.framer.com/api

export function Timeline_line() {
    const [twist, cycle] = useCycle(
        { scale: 0.5, rotate: 0 },
        { scale: 1, rotate: 90 }
    )
    //   mask: url(mask.svg);

    return (
        <Frame animate={twist} onTap={() => cycle()} size={"100%"}>
            <svg width="0" height="0">
                <defs
                    id="Symbols"
                    stroke="none"
                    stroke-width="1"
                    fill="none"
                    fill-rule="evenodd"
                >
                    <clipPath id="myClip" transform="scale(2 2)">
                        <path
                            d="M8.5,17 C3.80557963,17 0,13.1944204 0,8.5 C0,3.80557963 3.80557963,0 8.5,0 C13.1944204,0 17,3.80557963 17,8.5 C17,13.1944204 13.1944204,17 8.5,17 Z M8.5,115 C3.80557963,115 0,111.19442 0,106.5 C0,101.80558 3.80557963,98 8.5,98 C13.1944204,98 17,101.80558 17,106.5 C17,111.19442 13.1944204,115 8.5,115 Z M8.5,72 C6.01471863,72 4,69.9852814 4,67.5 C4,65.0147186 6.01471863,63 8.5,63 C10.9852814,63 13,65.0147186 13,67.5 C13,69.9852814 10.9852814,72 8.5,72 Z"
                            id="Combined-Shape"
                        ></path>
                        <rect
                            id="Rectangle"
                            x="7.5"
                            y="0"
                            width="2"
                            height="115"
                        ></rect>
                    </clipPath>
                </defs>
            </svg>

            <div
                style={{
                    width: "100px",
                    height: "100px",
                    clipPath: "url(#myClip)",
                    WebkitClipPath: "url(#myClip)",
                    background:
                        "linear-gradient(to right bottom, #430089, #82ffa1)",
                    position: "relative",
                }}
            ></div>
        </Frame>
    )
}
