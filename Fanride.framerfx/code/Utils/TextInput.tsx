import * as React from "react"
import { Stack, addPropertyControls, ControlType, Frame } from "framer"
import { colors } from "../canvas"
import { Icon } from "./Icon"

// Open Preview (CMD + P)
// API Reference: https://www.framer.com/api

export function TextInput(props) {
    const [valueState, setValueState] = React.useState(props.value)
    const inputStyles = {
        fontFamily: "Nunito",
        fontWeight: 900,
        fontSize: 20,
        padding: "0 0",
        color: colors.base,
        outline: "none",
        width: "100%",
        border: "none",
    }

    const wrapperStyles = {
        padding: "12px 20px",
        color: colors.base,
        border: `2px solid ${colors.grey}`,
        borderRadius: 25,
        background: "white",
    }

    const onChange = (event: React.ChangeEvent) => {
        const element = event.nativeEvent.target as HTMLInputElement

        const value = element.value

        setValueState(value)

        if (props.onValueChange) {
            props.onValueChange(value)
        }
    }

    return (
        <Stack
            {...props}
            width="100%"
            height="auto"
            style={wrapperStyles}
            gap={0}
            direction="horizontal"
        >
            <input
                type="text"
                width={"100%"}
                style={inputStyles}
                placeholder={props.placeholder}
                disabled={props.disabled}
                onChange={onChange}
                value={valueState}
            />
            <Frame height={"auto"} width={"25px"} background={"none"}>
                <Icon
                    width={"25px"}
                    stretch={true}
                    height={25}
                    icon={props.icon}
                    color={props.iconColor}
                />
            </Frame>
        </Stack>
    )
}

TextInput.defaultProps = {
    value: "",
    placeholder: "Type something…",
    width: "100%",
    height: 59,
    disabled: false,
    ...Stack.defaultProps,
}

addPropertyControls(TextInput, {
    ...(Stack as any).propertyControls,
    icon: {
        type: ControlType.String,
        defaultValue: "",
        title: "Icon",
    },
    iconColor: {
        type: ControlType.Color,
        defaultValue: "grey",
        title: "Icon color",
    },
    placeholder: {
        type: ControlType.String,
        defaultValue: "",
        title: "Placeholder",
    },
    value: {
        type: ControlType.String,
        defaultValue: "",
        title: "value",
    },
    disabled: {
        type: ControlType.Boolean,
        defaultValue: false,
        title: "Disabled",
    },
})
