import * as React from "react"
import { PropertyControls, ControlType } from "framer"
import styled from "styled-components"
import { colors } from "../canvas"

interface Props {
    value: string
    onValueChange: (value: string) => void
    placeholder: string
    textColor: string
    width: number
    disabled: boolean
    height: number
}

interface State {
    value: string
    valueFromProps: string
}

export class TextInputOldie extends React.Component<Partial<Props>, State> {
    static defaultProps = {
        value: "",
        placeholder: "Type something…",
        width: "100%",
        height: 59,
        disabled: false,
        textColor: "#000",
    }

    static propertyControls: PropertyControls<Props> = {
        value: { type: ControlType.String, title: "Value" },
        disabled: { type: ControlType.Boolean, title: "Disabled" },
        placeholder: { type: ControlType.String, title: "Default" },
    }

    state = {
        value: TextInputOldie.defaultProps.value,
        valueFromProps: TextInputOldie.defaultProps.value,
    }

    // Allow setting the Value from within the property panel.
    static getDerivedStateFromProps(props: Props, state: State) {
        if (props.value !== state.valueFromProps) {
            return { value: props.value, valueFromProps: props.value }
        }
    }

    onChange = (event: React.ChangeEvent) => {
        const element = event.nativeEvent.target as HTMLInputElement

        const value = element.value

        this.setState({ value })

        if (this.props.onValueChange) {
            this.props.onValueChange(value)
        }
    }

    StyledInput = styled.input`
      `

    render() {
        const { placeholder, textColor, disabled } = this.props

        const { value } = this.state

        return (
            <this.StyledInput
                onChange={this.onChange}
                value={value}
                placeholder={placeholder}
                disabled={this.props.disabled}
                style={{
                    ...style,
                }}
                type="text"
            />
        )
    }
}

const style: React.CSSProperties = {
    padding: "12px 20px",
    border: `2px solid ${colors.grey}`,
    borderRadius: 25,
    background: "white",
    fontFamily: "Nunito",
    fontWeight: 900,
    fontSize: 20,
    color: colors.base,
    width: "100%",
}
