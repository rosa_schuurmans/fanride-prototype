export const EventInspiration = [
    {
        type: "event",
        eventName: "Fontaines D.C.",
        user1: "https://randomuser.me/api/portraits/women/12.jpg",
        user2: "https://randomuser.me/api/portraits/women/52.jpg",
        user3: "https://randomuser.me/api/portraits/women/52.jpg",
        user4: "",
        eventDateDesc: "16 November in Paradiso",
        friend: "Freek",
        eventLocation: "Amsterdam",
        price: "€25",
        image: "https://randomuser.me/api/portraits/women/58.jpg",
        
    },
    {
        type: "event",
        eventName: "Pukkelpop Festival",
        user1: "https://randomuser.me/api/portraits/women/23.jpg",
        user2: "",
        user3: "",
        user4: "",
        eventDateDesc: "15 t/m 19 Augustus",
        friend: "Mario Cruz",
        eventLocation: "Amsterdam",
        price: "€15",
        image: "../design/images/empty.png",
    },

    {
        type: "nearby",
        image1: "../design/images/empty.png",
        image2: "../design/images/empty.png",
        image3: "../design/images/empty.png",
        image4: "../design/images/empty.png",
    },

    {
        type: "event",
        eventName: "The Fatal Flowers",
        user1: "https://randomuser.me/api/portraits/women/23.jpg",
        user2: "https://randomuser.me/api/portraits/women/42.jpg",
        user3: "https://randomuser.me/api/portraits/women/55.jpg",
        user4: "https://randomuser.me/api/portraits/women/87.jpg",
        eventDateDesc: "27 Juni in Paradiso",
        friend: "Pauline Lambert",
        eventLocation: "Amsterdam",
        price: "€25",
        image: "../design/images/empty.png",
    },
    {
        type: "branding",
        location: "Lowlands",
        title: "Lowlands virgin Heineken",
    },

    {
        type: "event",
        eventName: "Fontaines D.C.",
        user1: "https://randomuser.me/api/portraits/women/23.jpg",
        user2: "https://randomuser.me/api/portraits/women/42.jpg",
        user3: "https://randomuser.me/api/portraits/women/55.jpg",
        user4: "https://randomuser.me/api/portraits/women/87.jpg",
        eventDateDesc: "6 November in Paradiso",
        friend: "Freek",
        eventLocation: "Amsterdam",
        price: "€25",
        image: "../design/images/empty.png",
    },
]
export const Rides = [
    {
        hometown: "Hilversum",
        user: "Freek",
        dateDesc: "Pikt je om 10:30 op",
        description:
            "Hi! Dit wordt mijn 10e Lowlands. Ik kan je dus alle in’s & out’s vertellen!",
        price: "€25",
        Badge_1: '500km',
        Badge_2: 'talker',
        Badge_3: 'none',
        Badge_4: 'none',
        userAvatar: "https://randomuser.me/api/portraits/women/58.jpg",
        User_1: '3',
        User_2: 'none',
        User_3: 'none',
        User_4: 'none',
        car: 'VW Golf GTI, 2015'
    },
    {
      hometown: "Rotterdam",
      user: "Sanne",
      dateDesc: "Pikt je om 9:30 op",
      description:
          "Hi! Dit wordt mijn 10e Lowlands. Ik kan je dus alle in’s & out’s vertellen!",
      price: "€25",
      Badge_1: 'Talker',
      Badge_2: 'none',
      Badge_3: 'none',
      Badge_4: 'none',
      userAvatar: "https://randomuser.me/api/portraits/women/58.jpg",
      User_1: '4',
      User_2: 'none',
      User_3: 'none',
      User_4: 'none',
      car: 'VW Golf GTI, 2015'
    },
    {
        hometown: "Hilversum",
        user: "Freek",
        dateDesc: "Pikt je om 10:30 op",
        description:
            "Hi! Dit wordt mijn 10e Lowlands. Ik kan je dus alle in’s & out’s vertellen!",
        price: "€25",
        userAvatar: "https://randomuser.me/api/portraits/women/58.jpg",
    },
    {
        hometown: "Hilversum",
        user: "Freek",
        dateDesc: "Pikt je om 10:30 op",
        description:
            "Hi! Dit wordt mijn 10e Lowlands. Ik kan je dus alle in’s & out’s vertellen!",
        price: "€25",
        userAvatar: "https://randomuser.me/api/portraits/women/58.jpg",
    },
]

export const EventList = [
    {
      name: "Colours of Ostrava",
      date: '12 juni 2019'
    },
    {
      name: "Electric Castle",
      date: '12 juni 2019'
    },
    {
      name: "Zwarte Cross",
      date: '12 juni 2019'
    },
    {
      name: "Welcome to The Village",
      date: '12 juni 2019'
    },
    {
      name: "Lowlands Festival",
      date: '12 juni 2019'
    },
    {
      name: "Tomorrowland",
      date: '12 juni 2019'
    },
    {
      name: "Eilân Festival",
      date: '12 juni 2019'
    },
    {
      name: "Into the Woods",
      date: '12 juni 2019'
    },
    {
      name: "Appelpop",
      date: '12 juni 2019'
    },
    {
      name: "Smeerboel Festival",
      date: '12 juni 2019'
    },
    {
      name: "Draaimolen Festival:30",
      date: '12 juni 2019'
    },
    {
      name: "Parels van de Stad Festival",
      date: '12 juni 2019'
    },
    {
      name: "Nazomeren Festival",
      date: '12 juni 2019'
    },
    {
      name: "Sunglow Festival",
      date: '12 juni 2019'
    },
    {
      name: "Oxygen Festival",
      date: '12 juni 2019'
    },
    {
      name: "A Day at the Park",
      date: '12 juni 2019'
    },
    {
      name: "Summerlake Festival",
      date: '12 juni 2019'
    }
]
