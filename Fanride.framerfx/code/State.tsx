import { Data } from "framer"

export const appData = Data({
  openRide: 0,
  openEvent: 0
})

export const setOpenRide = val => {
  appData.openRide = val
}

export const setOpenEvent = val => {
  appData.openEvent = val
}

export const getStateVal = val => {
  return appData[val]
}
