import * as React from "react"
import {
    Frame,
    Override,
    Stack,
    ControlType,
    addPropertyControls,
} from "framer"
import { Topitem } from "./Topitem"
import { colors } from "../canvas"

export function Spotify(props) {
    return (
        <Frame {...props} width={"100%"} height={80} style={{overflow: 'hidden'}}>
            <iframe
                src={props.url}
                width="100%"
                height="100%"
                frameborder="0"
                allowtransparency="true"
                allow="encrypted-media"
            ></iframe>
        </Frame>
    )
}

Spotify.defaultProps = {
    background: "transparent",
    height: "100%",
    width: "100%",
    url: 'https://open.spotify.com/embed/playlist/1JvkJkoh69oAadYYL72Vy3',
    ...Frame.defaultProps,
}

addPropertyControls(Spotify, {
  url: {
      type: ControlType.String,
      defaultValue: 'https://open.spotify.com/embed/playlist/1JvkJkoh69oAadYYL72Vy3',
      title: "Url",
  },
})
