import * as React from "react"
import { Stack, ControlType, addPropertyControls } from "framer"
import { EventInspiration } from "../../Content"
import {
    Cards_Event_inspiration,
    Cards_Events_Branded,
    Cards_Nearby_users,
    Utils_Title
} from "../../Canvas"
import { GoTo } from "../../Overrides/Splash_Page"
import { setOpenEvent} from "../../State"

export function InspirationList(props) {
  
    return (
        <Stack
            {...props}
            width="100%"
        >
          
            {EventInspiration.map((item, index) => {
                if (item.type === "event") {
                    return (
                        <Cards_Event_inspiration
                            width="100%"
                            userNameGoing={`${item.friend} is going`}
                            userAvatars={{size: '100px'}}
                            onTap={() => { GoTo('event'); setOpenEvent(1) }}
                            {...item}
                        />
                    )
                }
                if (item.type === "nearby") {
                    return <Cards_Nearby_users {...item} width="100%" />
                }

                if (item.type === "branding") {
                    return <Cards_Events_Branded {...item} width="100%" />
                }
            })}
        </Stack>
    )
}

InspirationList.defaultProps = {
    placeholder: "undefined",
    width: "100%",
    label: "Hier kun je heen",
    distribution: "start",
    alignment:"center",
    gap: 32,
    ...Stack.defaultProps,
}

addPropertyControls(InspirationList, {
    ...(Stack as any).propertyControls,
    title: {
        type: ControlType.String,
        defaultValue: "Hier kun je heen",
        title: "title",
    },
})
