import * as React from "react"
import { Stack, ControlType, addPropertyControls } from "framer"
import { Rides } from "../../Content"
import { Cards_Ride } from "../../Canvas"
import { GoTo } from "../../Overrides/Splash_Page"
import { setOpenRide} from "../../State"

export function AvailableRides(props) {
    return (
        <Stack {...props} width="100%">
            {Rides.map((item, index) => {
                return (
                    <Cards_Ride
                        user={item.user}
                        price={item.price}
                        dateDesc={item.dateDesc}
                        hometown={item.hometown}
                        key={'av-ride-'+ index}
                        description={item.description}
                        onTap={() => { console.log('ta'); GoTo('ride'); setOpenRide(index) }}
                    />
                )
            })}
        </Stack>
    )
}
