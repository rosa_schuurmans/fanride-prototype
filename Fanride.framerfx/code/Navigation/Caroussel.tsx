import * as React from "react"
import { Override, Page, ControlType, addPropertyControls } from "framer"
import { Cards_Banner_Ride } from "../canvas"

export function Caroussel(props) {
    return (
        <Page {...props} width={"100%"}>
            <Cards_Banner_Ride width={'100%'}/>
            <Cards_Banner_Ride width={'100%'}/>
        </Page>
    )
}

Caroussel.defaultProps = {
    width: "100%",
    height: "100%",
    ...Page.defaultProps,
}

addPropertyControls(Caroussel, {
    ...Page.propertyControls,
})
