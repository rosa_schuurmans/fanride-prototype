import * as React from "react"
import {
    Override,
    Frame,
    Data,
    Stack,
    ControlType,
    addPropertyControls,
} from "framer"
import { EventList } from "../Content"
import { Cards_EventListItem } from "../canvas"

// States: show, closed, search

const data = Data({
    state: "close",
    eventSearchTerm: "",
    EventList: EventList,
})

export const closeModal = () => {
    data.state = "close"
}
export const openModal = () => {
    data.state = "open"
}

export const openSearch = () => {
    data.state = "search"
}

const staggerChildTransition = {
    staggerChildren: 0.2,
}

// Manage the height so when closed its not blocking others
export function ModalWrapper(): Override {
    const container = {
        close: {
            height: "80px",
        },
        search: {
            // height: 500,
            height: "100%",
        },
        open: {
            // height: 309
            height: "100%",
        },
    }
    return {
        variants: container,
        initial: data.state,
        transition: {
            duration: 0,
        },
        bottom: 0,
        animate: data.state,
    }
}
export function WhiteDrawer(): Override {
    const container = {
        close: {
            y: "110%",
            opacity: 1,
            height: 309,
        },
        search: {
            y: "0%",
            opacity: 1,
            height: 500,
        },
        open: {
            y: "0%",
            opacity: 1,
            height: 309,
        },
    }
    return {
        variants: container,
        initial: data.state,
        animate: data.state,
    }
}

export function BackDrop(): Override {
    const item = {
        close: { opacity: 0 },
        open: { opacity: 1 },
    }
    return {
        variants: item,
        initial: data.state,
        animate: data.state,
        onTap: closeModal,
    }
}

export function OpenDrawer(): Override {
    return {
        onTap: openModal,
    }
}

// set the position based on the state, make the eventlist available to the component
export function EventFrame(): Override {
    return {
        variants: {
            open: {
                top: 86,
            },
            close: {
                top: 86,
            },
            search: {
                top: 24,
            },
        },
        height: "80%",
        EventList: data.EventList,
    }
}
// trigger the open
export function EventInputFrame(): Override {
    return {
        onTap() {
            openSearch()
        },
    }
}

// manage the input field, store the value
export function EventInput(): Override {
    return {
        value: data.eventSearchTerm,
        disabled: data.state !== "search",
        onValueChange: value => {
            data.eventSearchTerm = value
            data.EventList = EventList.filter(item => {
                return (
                    item.name.toLowerCase().indexOf(value.toLowerCase()) !== -1
                )
            })
        },
    }
}

// manage the input field
export function hideDuringSearch(): Override {
    return {
        variants: {
            open: {
                opacity: 1,
            },
            close: {
                opacity: 1,
            },
            search: {
                opacity: 0,
            },
        },
    }
}

// manage the input field
export function SearchScroll(): Override {
    return {
        variants: {
            open: {
                opacity: 0,
                visibile: false,
            },
            close: {
                opacity: 0,
                visibile: false,
            },
            search: {
                opacity: 1,
                visible: true,
            },
        },
    }
}

// the list component
export function SearchList(props) {
    const [state, setState] = React.useState({
        EventList: props.EventList,
    })

    if (state.EventList !== data.EventList) {
        setState({
            EventList: data.EventList,
        })
    }

    const makeChoice = selection => {
        data.eventSearchTerm = selection.name
        openModal()
    }
    return (
        <Stack {...props}>
            {state.EventList.length == 0 && (
                <h2 onClick={makeChoice}>There are no results</h2>
            )}
            {state.EventList.map(item => {
                return (
                    <Cards_EventListItem
                        {...item}
                        onTap={() => {
                            makeChoice(item)
                        }}
                    />
                )
            })}
        </Stack>
    )
}

SearchList.defaultProps = {
    inputText: "",
    EventList: EventList,
    onValueChange: () => null,
    ...Stack.defaultProps,
}

addPropertyControls(SearchList, {
    ...Stack.propertyControls,
})
