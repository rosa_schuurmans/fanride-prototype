import * as React from "react"
import { Frame, addPropertyControls, ControlType } from "framer"
// Open Preview (CMD + P)
// API Reference: https://www.framer.com/api

export function Dot(props) {
    const dotStyle = {
        top: `${props.dotProgress}%`,
        left: "-6px",
        backgroundColor: "transparent",
        width: "12px",
        height: "12px",
        radius: "100%",
        border: `2px solid ${props.dotOuter}`,
        boxShadow: "0 0 0 2px rgba(37, 211, 167, 1)",
    }
    const dotInnerStyle = {
        backgroundColor: props.dotInner,
        width: "6px",
        height: "6px",
        left: "1px",
        top: "1px",
        radius: "100%",
    }
    return (
        <Frame
            style={dotStyle}
            animate={{
                boxShadow: "0 0 0 12px rgba(37, 211, 167, 0)",
            }}
            transition={{
                loop: Infinity,
                duration: 1,
            }}
        >
            <Frame style={dotInnerStyle}></Frame>
        </Frame>
    )
}

Dot.defaultProps = {
    dotInner: "rgb(32, 79, 139)",
    dotOuter: "rgb(189, 223, 73)",
}

addPropertyControls(Dot, {
    dotInner: {
        type: ControlType.Color,
        title: "dotInner",
        defaultValue: "#fff",
    },
    dotOuter: {
        type: ControlType.Color,
        title: "dotOuter",
        defaultValue: "#fff",
    },
    dotProgress: {
        type: ControlType.Number,
        title: "Dot Progress",
        defaultValue: 50,
        min: 0,
        max: 100,
        unit: "%",
    },
})
