import * as React from "react"
import { Frame, addPropertyControls, ControlType } from "framer"
import { Dot } from "./Dot"
// Open Preview (CMD + P)
// API Reference: https://www.framer.com/api

export function BarDot(props) {
    const outerStyle = {
        width: "2px",
        height: "100%",
        backgroundColor: "transparent",
    }

    return (
        <Frame style={outerStyle}>
            <Dot {...props} />
        </Frame>
    )
}

BarDot.defaultProps = {
    dotInner: "rgb(32, 79, 139)",
    dotOuter: "rgb(189, 223, 73)",
}

addPropertyControls(BarDot, {
    dotInner: {
        type: ControlType.Color,
        title: "dotInner",
        defaultValue: "#fff",
    },
    dotOuter: {
        type: ControlType.Color,
        title: "dotOuter",
        defaultValue: "#fff",
    },
    dotProgress: {
        type: ControlType.Number,
        title: "Dot Progress",
        defaultValue: 50,
        min: 0,
        max: 100,
        unit: "%",
    },
})
