import * as React from "react"
import { Override, Data } from "framer"

// @ts-ignore
import { getBadgeByName, getRideById, getProfileById, activatedRides } from "../Content"
// @ts-ignore
import { getAppData, setAppData } from "../State"
// @ts-ignore
import { createStore } from "../Store"
import { scrollToTopFunc } from "../scroll/title_scroll"

const useStore = createStore({ currentPage: 0 })

const data = Data({
    currentPage: 0,
    ride: {},
    user: {}
})

export function modal(props): Override {
    const [store, setStore] = useStore()
    
    React.useEffect(() => {
        // @ts-ignore
        setStore({ currentPage: 0 })
    }, [])

    return {
        currentPage: store.currentPage,
        variants: {
            0: {
                background: "white",
            },
            1: {
                background: "white",
            },
        },
        initial: "0",
        animate: store.currentPage.toString(),
    }
}

export function page(props): Override {
    const [store, setStore] = useStore()
    return {
        currentPage: store.currentPage,
    }
}

export function nextPageButton(props): Override {
    const [store, setStore] = useStore()

    // @ts-ignore
    return {
        visible: store.currentPage === 0 ? true : false,
        onTap() {
            setStore({ currentPage: store.currentPage + 1 })
        },
    }
}


export function confirmButton(props): Override {
    const [store, setStore] = useStore()
    // @ts-ignore
    return {
        visible: store.currentPage === 1 ? true : false,
        onTap() {
            
        },
    }
}

export function getData(props): Override {
    data.ride = getRideById(getAppData("openRide"))

    // @ts-ignore
    return {
        ...data.ride,
        kmPriceTitle: `${data.ride.km} KM of the total ride`,
        kmPriceValue: `€${data.ride.kmPrice}`
    }
}

export function backButton(props): Override {
    const [store, setStore] = useStore()
    return {
        currentPage: store.currentPage,
        variants: {
            0: {
                opacity: 0,
                left: 36,
            },
            1: {
                opacity: 1,
                left: 16,
            },
        },
        initial: "0",
        animate: store.currentPage.toString(),
        onTap() {
            setStore({ currentPage: store.currentPage - 1 })
        },
    }
}
