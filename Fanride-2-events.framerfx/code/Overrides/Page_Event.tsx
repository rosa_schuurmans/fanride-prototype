import * as React from "react"
import { Override, Data } from "framer"

// @ts-ignore
import { getEventById, getProfileById, rides } from "../Content"
// @ts-ignore
import { getAppData, setAppData } from "../State"
// @ts-ignore
import { scrollToTopFunc } from "../scroll/title_scroll"

const data = Data({
    items: {},
    ride: {},
    profile: {},
    badgesDOM: [],
})
export function getEventPageData(props): Override {
    const event = getEventById(getAppData("openEvent"))

    return {
        ...event,
        eventPageTitle: "Your rides to " + event.eventName,
        noridetext: `There are currently no rides available for ${event.eventName}. 
        
Subscribe to our ride alerts and you’ll be the first to know when a new ride is available!`,
    }
}

export function ClickOnEvent(props, state): Override {
    data.items[props.id] = props.id

    const index = Object.keys(data.items).indexOf(props.id)
    data.ride = rides[index]
    //   @ts-ignore
    data.profile = getProfileById(data.ride.userId)

    return {
        ...data.ride,
        ...data.profile,
        onTap() {
            scrollToTopFunc()
            setAppData("openRide", Object.keys(data.items).indexOf(props.id))
        },
    }
}

export function getCurrentPageBadges(props, state): Override {
    data.badgesDOM[props.id] = props.id

    const index = Object.keys(data.badgesDOM).indexOf(props.id)
    const userId = rides[Math.floor(index / 4)].userId;
    const badge = getProfileById(userId).badges[index % 4]
    

    return {
        socialBadge: badge,
    }
}
