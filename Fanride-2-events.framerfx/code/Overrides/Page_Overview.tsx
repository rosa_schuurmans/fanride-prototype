import * as React from "react"
import { Override, Data } from "framer"

// @ts-ignore
import { getEventById, getRideById, activatedRides } from "../Content"
// @ts-ignore
import { setAppData, resetAppData } from "../State"
// @ts-ignore

const data = Data({
    items: {},
})

export function ClickOnEvent(props, state): Override {
    data.items[props.id] = props.id

    const index = Object.keys(data.items).indexOf(props.id)
    const event = getEventById(index)
    //   @ts-ignore
    return {
        ...event,
        onTap() {
            setAppData("openEvent", Object.keys(data.items).indexOf(props.id))
        },
    }
}

export function clickOnActivatedRide0(props, state): Override {
    React.useEffect(() => {
        // @ts-ignore
        resetAppData()
    }, [])
    return {
        onTap() {
            setAppData("openActivatedRide", 0)
        },
    }
}
export function clickOnActivatedRide1(props, state): Override {
    return {
        onTap() {
            setAppData("openActivatedRide", 1)
        },
    }
}
export function clickOnActivatedRide2(props, state): Override {
    return {
        onTap() {
            setAppData("openActivatedRide", 2)
        },
    }
}
