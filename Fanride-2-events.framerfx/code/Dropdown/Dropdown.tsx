import * as React from "react"
import { Frame, Stack, ControlType, addPropertyControls } from "framer"
import { Topitem } from "./Topitem"
import { colors } from "../canvas"
import { appData } from "../State"

const textStyles = {
    background: "white",
    fontFamily: "Nunito",
    fontWeight: 900,
    fontSize: 20,
    padding: "0 0",
    outline: "none",
    border: "none",
}

export function Dropdown(props) {
    const [activeIndex, setActiveIndex] = React.useState(0)
    const [openComponent, setComponentOpen] = React.useState(false)

    appData.form[props.valueName] = props.options[activeIndex]

    // Parent variants
    const containerState = {
        closed: { rotate: 0},
        open: {
            rotate: 0,
            transition: {
                staggerChildren: 0.02,
            },
        },
    }

    const makeChoice = index => {
        setActiveIndex(index)
        toggleOpenState()
        appData.form[props.valueName] = props.options[index]
    }

    const toggleOpenState = () => {
        setComponentOpen(!openComponent)
    }

    const listStyles = {
        borderRadius: 25,
        overflow: "hidden",
        border: `2px solid ${props.borderColor}`,
    }

    const choiceLabelStyles = {
        ...textStyles,
        paddingRight: "18px",
        paddingLeft: "16px",
        display: 'flex',
        alignContent: 'center'
    }
    const containerStyles = {
        borderRadius: 25,
        overflow: "hidden",
        background: "white",
        border: `2px solid ${props.borderColor}`,
    }

    return (
        <Frame
            {...props}
            width="100%"
            variants={{
                open: { height: 50 * props.options.length },
                closed: {
                    height: 50,
                },
            }}
            value="trol"
            initial="closed"
            animate={openComponent ? "open" : "closed"}
            style={containerStyles}
        >
            <Topitem
                getTap={toggleOpenState}
                openComponent={openComponent}
                text={props.options[activeIndex]}
            />
            <Stack
                height={500}
                top={50}
                width="100%"
                direction="vertical"
                alignment="center"
                gap={0}
                variants={containerState}
                initial="closed"
                animate={openComponent ? "open" : "closed"}
            >
                {props.options.map((option, index) => {
                    return (
                        index !== activeIndex && (
                            <Frame
                                key={`key_${index}`}
                                width="100%"
                                height={50}
                                variants={{
                                    closed: { y: -50 * (index + 1) },
                                    open: {
                                        y: 0,
                                    },
                                }}
                                
                                style={choiceLabelStyles}
                                onTap={() => makeChoice(index)}
                            >
                                <span>{option}</span>
                            </Frame>
                        )
                    )
                })}
            </Stack>
        </Frame>
    )
}

Dropdown.defaultProps = {
    value: ["Paris"],
    options: ["Paris", "New York", "London", "Hong Kong"],
    height: "100%",
    borderColor: "red",
    width: "100%",
    valueName: "Dropdown",
}

addPropertyControls(Dropdown, {
    value: {
        type: ControlType.Array,
        propertyControl: {
            type: ControlType.String,
        },
        defaultValue: ["Paris"],
        title: "Value",
    },
    options: {
        type: ControlType.Array,
        propertyControl: {
            type: ControlType.String,
        },
        defaultValue: ["Paris", "New York", "London", "Hong Kong"],
        title: "Options",
    },
    borderColor: {
        type: ControlType.Color,
        defaultValue: "red",
        title: "Border color",
    },
    valueName: {
        type: ControlType.String,
        defaultValue: "Dropdown",
        title: "Value name",
    },
})
