import * as React from "react"
import { Frame, addPropertyControls, ControlType, Data } from "framer"
// @ts-ignore
import { Icon } from "./Icon"

// Open Preview (CMD + P)
// API Reference: https://www.framer.com/api

const data = Data({
    tapped: false,
})

export function Ride_alert(props) {
    const [currentTapped, setTapped] = React.useState(false)

    const onTap = () => {
        setTapped(!currentTapped)
    }
    const styles = {
        width: "100%",
        height: "100%",
        radius: "15px",
        fontFamily: "Nunito",
        fontSize: "18px",
        color: "white",
        fontWeight: 800,
    }

    const iconFrame = {
        width: "24px",
        height: "24px",
        left: "12px",
        top: "calc(50% - 12px)",
        background: "transparent",
    }

    const variants = {
        tapped: {
            background: props.secondColor,
        },
        untapped: {
            background: props.initialColor,
        },
    }
    return (
        <Frame
            onTap={onTap}
            style={styles}
            variants={variants}
            initial={"untapped"}
            animate={currentTapped ? "tapped" : "untapped"}
        >
            <Frame
                center
                style={{
                    textAlign: "center",
                    display: "block",
                    background: "transparent",
                    height: "auto",
                }}
            >
                {currentTapped ? props.secondLabel : props.initialLabel}
            </Frame>
            <Frame style={iconFrame}>
                <Icon
                    color={"white"}
                    icon={currentTapped ? props.secondIcon : props.initialIcon}
                />
            </Frame>
        </Frame>
    )
}
Ride_alert.defaultProps = {
    initialColor: "rgb(32, 79, 139)",
    secondColor: "rgb(189, 223, 73)",
    initialLabel: "first",
    initialIcon: "add",
    secondIcon: "close",
    secondLabel: "second",
}

addPropertyControls(Ride_alert, {
    initialColor: {
        type: ControlType.Color,
        defaultValue: "#fff",
    },
    secondColor: {
        type: ControlType.Color,
        defaultValue: "#fff",
    },
    initialLabel: {
        type: ControlType.String,
    },
    secondLabel: {
        type: ControlType.String,
    },
    initialIcon: {
        type: ControlType.String,
        defaultValue: "add",
    },
    secondIcon: {
        type: ControlType.String,
        defaultValue: "close",
    },
})
