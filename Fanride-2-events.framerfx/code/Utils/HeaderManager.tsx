import * as React from "react"
import { Frame } from "framer"

// @ts-ignore
import { Helmet } from "react-helmet"

// Open Preview (CMD + P)
// API Reference: https://www.framer.com/api

export function FontManager() {
    return (
        <Frame size={"100%"}>
            <Helmet>
                <link
                    href={`https://fonts.googleapis.com/css?family=Nunito:300,400,700,800,900&display=swap`}
                    rel="stylesheet"
                />
                <link
                    rel="apple-touch-icon"
                    sizes="180x180"
                    href="/design/images/appicon/apple-touch-icon.png"
                />
                <link
                    rel="icon"
                    type="image/png"
                    sizes="32x32"
                    href="/design/images/appicon/favicon-32x32.png"
                />
                <link
                    rel="icon"
                    type="image/png"
                    sizes="16x16"
                    href="/design/images/appicon/favicon-16x16.png"
                />
                <link
                    rel="manifest"
                    href="/design/images/appicon/manifest.json"
                />
                <link
                    rel="mask-icon"
                    href="/design/images/appicon/safari-pinned-tab.svg"
                    color="#000000"
                />

                <meta name="apple-mobile-web-app-title" content="Fanride" />
                <meta
                    name="apple-mobile-web-app-status-bar-style"
                    content="default"
                />
                <meta name="application-name" content="Fanride" />
                <meta name="msapplication-TileColor" content="#000000" />
                <meta name="theme-color" content="#ffffff" />
                <link
                    href="https://fonts.googleapis.com/icon?family=Material+Icons"
                    rel="stylesheet"
                />

                <style></style>
            </Helmet>
        </Frame>
    )
}
