import * as React from "react"
import { PropertyControls, ControlType, Frame } from "framer"
import styled from "styled-components"

// @ts-ignore
import { Icon } from "./Icon"

interface Props {
    value: string
    onValueChange: (value: string) => void
    placeholder: string
    color: string
    ciki: string
    iconName: string
    iconColor: string
    textColor: string
    borderColor: string
    width: number
    disabled: boolean
    height: number
}

interface State {
    value: string
    valueFromProps: string
}

export class TextInput extends React.Component<Partial<Props>, State> {
    static defaultProps = {
        value: "",
        placeholder: "Type something…",
        width: "100%",
        height: 59,
        disabled: false,
        iconColor: "red",
        iconName: "",
        color: "red",
        borderColor: "lightgrey",
        textColor: "#000",
    }

    static propertyControls: PropertyControls<Props> = {
        value: { type: ControlType.String, title: "Value" },
        disabled: { type: ControlType.Boolean, title: "Disabled" },
        placeholder: { type: ControlType.String, title: "Default" },
        color: { type: ControlType.Color, title: "color" },
        borderColor: { type: ControlType.Color, title: "borderColor" },
        iconColor: { type: ControlType.Color, title: "icon color" },
        iconName: { type: ControlType.String, title: "icon name" },
    }

    state = {
        value: TextInput.defaultProps.value,
        valueFromProps: TextInput.defaultProps.value,
    }

    // Allow setting the Value from within the property panel.
    static getDerivedStateFromProps(props: Props, state: State) {
        if (props.value !== state.valueFromProps) {
            return { value: props.value, valueFromProps: props.value }
        } else {
            return {}
        }
    }

    onChange = (event: React.ChangeEvent) => {
        const element = event.nativeEvent.target as HTMLInputElement

        const value = element.value

        this.setState({ value })

        if (this.props.onValueChange) {
            this.props.onValueChange(value)
        }
    }

    StyledInput = styled.input`
      `

    render() {
        const { placeholder, textColor, disabled } = this.props

        const { value } = this.state

        return (
            <Frame height={"auto"} background={"transparent"} width={"100%"}>
                <this.StyledInput
                    onChange={this.onChange}
                    value={value}
                    placeholder={placeholder}
                    style={{
                        WebkitAppearance: "none",
                        MozAppearance: "none",
                        appearance: "none",
                        padding: "12px 20px",
                        border: `2px solid ${this.props.borderColor}`,
                        borderRadius: 25,
                        background: "white",
                        fontFamily: "Nunito",
                        fontWeight: 900,
                        fontSize: 20,
                        outline: "none !important",
                        color: this.props.color,
                        width: "100%",
                        pointerEvents: this.props.disabled ? "none" : "all",
                    }}
                    type="text"
                />
                <Frame
                    width={24}
                    height={24}
                    top={17}
                    right={18}
                    background={"transparent"}
                >
                    <Icon
                        color={this.props.iconColor}
                        icon={this.props.iconName}
                    />
                </Frame>
            </Frame>
        )
    }
}
