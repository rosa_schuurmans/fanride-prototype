import * as React from "react"
import { Frame, useCycle, ControlType, addPropertyControls } from "framer"
import { url } from "framer/resource"
// @ts-ignore
import { setAppData } from "../State"

// Open Preview (CMD + P)

// API Reference: https://www.framer.com/api

export function Badge(props) {
    const badgeUrl =
        props.badgeType === "social"
            ? `./design/images/badges/badge-${props.socialBadge}.png`
            : `./design/images/car/${props.carBadge}.png`
    const frameStyles = {
        width: "100%",
        height: "100%",
        backgroundColor: "transparent",
        backgroundSize: "contain",
        backgroundRepeat: "no-repeat",
        backgroundPosition: "center center",
    }
    
    const badgeName = props.badgeType === "social" ? props.socialBadge : props.carBadge

    const onTap = () => {
        setAppData("openBadge", badgeName)
    }

    return <Frame style={frameStyles} image={url(badgeUrl)} onTap={onTap} />
}

Badge.defaultProps = {
    badgeType: "social",
    socialBadge: "500km",
    carBadge: "airconditioning",
}

addPropertyControls(Badge, {
    badgeType: {
        type: ControlType.SegmentedEnum,
        options: ["social", "car"],
        optionTitles: ["social", "car"],
    },

    socialBadge: {
        type: ControlType.Enum,
        options: [
            "500km",
            "alwaysontime",
            "cleancar",
            "gamer",
            "talker",
            "rocker",
        ],
        hidden(props) {
            return props.badgeType !== "social"
        },
    },
    carBadge: {
        type: ControlType.Enum,
        options: [
            "airconditioning",
            "hybrid-electric",
            "no-eating",
            "no-smoking",
        ],
        hidden(props) {
            return props.badgeType !== "car"
        },
    },
})
