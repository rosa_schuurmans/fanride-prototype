import * as React from "react"
import {
    Data,
    Page,
    Frame,
    Override,
    ControlType,
    addPropertyControls,
} from "framer"

import { Card_Banner_ride } from "../canvas"
//@ts-ignore
import { activatedRides, getRideById } from "../Content"

const data = Data({
    currentIndex: 0,
    segments: {},
})

export function BannerPageWrapper(props): Override {
    return {
        onChangePage(currentIndex: number = 0) {
            data.currentIndex = currentIndex
        },
    }
}

export function dot(props): Override {
    let opacity = 0.5

    data.segments[props.id] = props.id

    if (Object.keys(data.segments)[data.currentIndex] === props.id) {
        opacity = 1
    }

    return {
        opacity: opacity,
    }
}
