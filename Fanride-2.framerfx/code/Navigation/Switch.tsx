import { Override, Data } from "framer"

export const switchData = Data({
    active: "left",
})

const transition = {
    duration: ".2",
}

export function Bar(): Override {
    return {
        variants: {
            left: {
                x: "0%",
            },
            right: {
                x: "100%",
            },
        },

        initial: switchData.active,
        animate: switchData.active,
    }
}

export function displayWhenLeft(): Override {
    return {
        visible: switchData.active === "left" ? true : false,
    }
}

export function displayWhenRight(): Override {
    return {
        visible: switchData.active === "right" ? true : false,
    }
}

export function wrapper(): Override {
    return {
        onTap() {
            switchData.active = switchData.active === "left" ? "right" : "left"
        },
    }
}

export function visibleRight(): Override {
    return {
        transition: transition,
        variants: {
            left: {
                opacity: 0,
            },
            right: {
                opacity: 1,
            },
        },
        initia: switchData.active,
        animate: switchData.active,
    }
}
export function visibleLeft(): Override {
    return {
        transition: transition,
        variants: {
            left: {
                opacity: 1,
            },
            right: {
                opacity: 0,
            },
        },
        initia: switchData.active,
        animate: switchData.active,
    }
}
