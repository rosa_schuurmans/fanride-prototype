// @ts-ignore
import { Override, Data, motionValue, useTransform, useAnimation } from "framer"
import * as React from "react"
let controls

const data = Data({
    contentOffsetY: motionValue(0),
})
export function scrollToTopFunc() {
    if(controls) {
      controls.start({ y: 0, transition: { duration: 0 } })
    }
}

export function scrollToTopOnMount(): Override {
  React.useEffect(() => {
      // @ts-ignore
      scrollToTopFunc()
  }, [])

    return {
        
      
    }
}

export function scrollComponent(): Override {
    controls = useAnimation()

    return {
        contentOffsetY: data.contentOffsetY,
        scrollAnimate: controls,
    }
}

export function headerTitle(): Override {
    const y = useTransform(data.contentOffsetY, [-170, -200], [-20, 0], {
        clamp: true,
    })

    const opacity = useTransform(data.contentOffsetY, [-170, -200], [0, 1], {
        clamp: true,
    })
    return {
        y: y,
        opacity: opacity,
    }
}
export function headerLogo(): Override {
    const y = useTransform(data.contentOffsetY, [-170, -180], [1, 20], {
        clamp: true,
    })

    const opacity = useTransform(data.contentOffsetY, [-170, -180], [1, 0], {
        clamp: true,
    })

    return {
        y: y,
        opacity: opacity,
    }
}

export function image(): Override {
    const y = useTransform(data.contentOffsetY, [-10, -100], [1, 1.15], {
        clamp: true,
    })

    return {
        scale: y,
        y: y,
    }
}
export function imageTitle(): Override {
    const opacity = useTransform(data.contentOffsetY, [0, -200], [1, 0], {
        clamp: false,
    })
    const y = useTransform(data.contentOffsetY, [200, -200], [40, -50], {
        clamp: false,
    })
    return {
        opacity: opacity,
        y: y,
    }
}

export function stickyHeader(props): Override {
    const navHeight = 60
    const travel = props.top - navHeight
    const y = useTransform(
        data.contentOffsetY,
        [0, -travel, -travel * 2],
        [0, 0, travel],
        {
            clamp: false,
        }
    )
    return {
        y: y,
    }
}
