import { Data } from "framer"

export const appData = Data({
    form: {
        persons: "1 Persoon",
        location: "Larenseweg 32, Hilversum",
        event: "Lowlands Festival 2019",
        direction: "retour",
    },
    openRide: 0,
    openEvent: 0,
    openProfile: 1,
    didReserve: false,
    openBadge: '500km'
})

export const setAppData = (prop, val) => {
    appData[prop] = val
}

export const getAppData = prop => {
    return appData[prop]
}
export const resetAppData = prop => {
    setAppData('openRide', 0);
    setAppData('openEvent', 0);
    setAppData('openProfile', 0);
    setAppData('openBadge', '500km');
    setAppData('openActivatedRide', 0);
}
