import * as React from "react"
import { Override, Data } from "framer"

// @ts-ignore
import { getBadgeByName } from "../Content"
// @ts-ignore
import { getAppData, setAppData } from "../State"

interface data {
    badge: any
}

const data = Data({
    badge: {},
})

export function getBadgeData(props): Override {
    const badgeName = getAppData("openBadge")
    //   @ts-ignore
    data.badge = getBadgeByName(badgeName)

    return {
        ...data.badge,
        badgeType: "social",
        socialBadge: badgeName,
    }
}
