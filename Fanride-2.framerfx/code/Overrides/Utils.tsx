import { Override, Data } from "framer"

// @ts-ignore
import { scrollToTopFunc } from "../scroll/title_scroll"

export function HeightAuto(): Override {
    return {
        height: "auto",
        eventName: "a liitt bit longer name then normal",
    }
}
export function WidthAuto(): Override {
    return {
        width: "auto",
        eventName: "a liitt bit longer name then normal",
    }
}

export function scrollToTop(props, state): Override {
    return {
        onTap() {
            scrollToTopFunc()
        },
    }
}
