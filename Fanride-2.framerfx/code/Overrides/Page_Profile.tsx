import * as React from "react"
import { Override, Data } from "framer"

// @ts-ignore
import { getProfileById } from "../Content"
// @ts-ignore
import { getAppData, setAppData } from "../State"

interface data {
    items: any
    ride: any
    profile: any
}

const data = Data({
    items: {},
    ride: {},
    profile: {},
    badgesDOM: []
})

export function getProfilePageData(props): Override {
    //   @ts-ignore
    data.profile = getProfileById(getAppData("openProfile"))

    return {
        ...data.profile,
        // @ts-ignore
        carTitle: `${data.profile.user}'s car`,
        pageTitle: "Your ride to Lowlands Festival"
    }
}

export function getCurrentPageBadges(props, state): Override {
    data.badgesDOM[props.id] = props.id

    const index = Object.keys(data.badgesDOM).indexOf(props.id)
    data.profile = getProfileById(getAppData("openProfile"))
    const badge = data.profile.badges[index]
    

    return {
        socialBadge: badge,
    }
}


export function ClickOnUser(props, state): Override {
    return {
        onTap() {
            // @ts-ignore
            setAppData("openProfile", data.profile.id)
        },
    }
}
