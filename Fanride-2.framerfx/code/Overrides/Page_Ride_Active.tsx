import {
    Override,
    Data,
    motionValue,
    useTransform,
    useAnimation,
    ControlType,
    addPropertyControls,
    Scroll,
} from "framer"
import * as React from "react"
//@ts-ignore
import { createStore } from "../Store"
//@ts-ignore
import { getActiveRideById, getProfileById, steps } from "../Content"
// @ts-ignore
import { setAppData } from "../State"

const useStore = createStore({ isPastLimit: false, step: 0, canDisplayNext: false })

let mapControls
let pageControls

const timelineItemSteps = [3, 5, 6, 7, 9, 11, 13]

const data = Data({
    contentOffsetY: motionValue(0),
    isPastLimit: false,
    timeline: [],
    timelineItems: [],
    timelineItemsBars: [],
    step: 0,
    badgesDOM: [],
    badges: [],
})

export function title(): Override {
    const animationState = {
        y: data.isPastLimit ? 0 : 20,
        opacity: data.isPastLimit ? 1 : 0,
    }
    return {
        initial: animationState,
        animate: animationState,
    }
}

export function logo(): Override {
    const animationState = {
        y: data.isPastLimit ? -40 : 0,
        opacity: data.isPastLimit ? 0 : 1,
    }

    return {
        initial: animationState,
        animate: animationState,
        transition: {
            duration: ".2",
        },
    }
}

export function bar(): Override {
    const progressSteps = ["10", "20"]
    return {
        progress: progressSteps[data.step],
    }
}

export function setOpenProfileBram(): Override {
    return {
        onTap() {
            setAppData("openProfile", 0)
        },
    }
}
export function setOpenProfileSanne(): Override {
    return {
        onTap() {
            setAppData("openProfile", 1)
        },
    }
}
export function setOpenProfileMarije(): Override {
    return {
        onTap() {
            setAppData("openProfile", 4)
        },
    }
}

export function getCurrentPageBadges(props, state): Override {
    data.badgesDOM[props.id] = props.id

    const index = Object.keys(data.badgesDOM).indexOf(props.id)
    const userId = 0
    //   @ts-ignore
    data.badges = getProfileById(userId).badges
    //   @ts-ignore
    const badge = data.badges[index]

    return {
        socialBadge: badge,
    }
}

export function CTAPageFlipper(props): Override {
    return {
        currentPage: data.step,
        animateCurrentPageUpdate: false,
    }
}

export function triggerNextStep(props): Override {
    const [store, setStore] = useStore()
    const [label, setLabel] = React.useState('next step')

    return {
      variants: {
        can: {
          right: 11,
          opacity: 1
        },
        cannot: {
          right: -170,
          opacity: 0
        }
      },
      Label: label,
      initial:  'cannot',
      animate: store.canDisplayNext ? 'can' : 'cannot',
        onTap() {
          // set label to restart at the last step
          // increate the step on tap
          // if step is the last step, next should trigger the restart.
          
            data.step++
            
            if(data.step === 5) {
              setLabel('restart');
            }
            if(data.step > 5) {
              data.step  = 0;
              setLabel('next step');
            }
            
            setStore({ step: data.step, canDisplayNext: false })
            
            pageControls.start({ y: steps[data.step].scrollProgress * -1, 
              transition: { 
                duration: 0.3, 
                ease: "easeInOut" 
              } })
        },
    }
}

export function stickyHeader(): Override {
    const [store, setStore] = useStore()
    const animationState = {
        y: data.isPastLimit ? 0 : -20,
        opacity: data.isPastLimit ? 1 : 0,
    }
    return {
        initial: animationState,
        animate: animationState,
    }
}

export function progressController(): Override {
    const [store, setStore] = useStore()

    return {
        ...steps[data.step],
        onTap() {
          setStore({canDisplayNext: true })
        }
    }
}

export function timelineItem(props, state): Override {
    const [store, setStore] = useStore()
    data.timeline = getActiveRideById(0).timeline
    data.timelineItems[props.id] = props.id

    const index = Object.keys(data.timelineItems).indexOf(props.id)
    const timelineItem = data.timeline[index]

    return {
        ...timelineItem,
        opacity: timelineItemSteps[data.step] <= index ? 1 : 0.4,
        onTap() {
            // setAppData("openProfile", data.profile.id)
        },
    }
}

export function activeBar(props, state): Override {
    const [store, setStore] = useStore()
    data.timelineItemsBars[props.id] = props.id
    const index = Object.keys(data.timelineItemsBars).indexOf(props.id)
    return {
        opacity: timelineItemSteps[data.step] == index ? 1 : 0,
    }
}

export function map(): Override {
    mapControls = useAnimation()
    React.useEffect(() => {
        mapControls.start({ x: -180, y: -180, transition: { 
          duration: 0
        } })
    }, [])
    return {
        scrollAnimate: mapControls,
    }
}

export function userCarAnimation(): Override {
    return {
        animate: {
            x: 20,
            y: 200,
        },
        transition: {
            duration: 8,
        },
    }
}

export function cameraMovement(): Override {
    return {
        animate: {
            x: 20,
            y: 40,
        },
        transition: {
            yoyo: Infinity,
            duration: 3,
        },
    }
}

export function cameraScan(): Override {
    return {
        animate: {
            scale: 1.1,
        },
        transition: {
            yoyo: 3,
            duration: 0.1,
            delay: 3,
        },
    }
}

export function successframe(): Override {
    return {
        opacity: 0,
        animate: {
            opacity: 1,
        },
        transition: {
            delay: 3,
        },
    }
}

export function triggerSpotify(): Override {
  
    return {
        onTap() {
            // window.open(
            //     "https://open.spotify.com/playlist/6vHcuuV0uvyscpxsMVsUqU?si=RRG2uxknT1qaDyN2jimZLw", "_blank"
            // )
            // return;
        },
    }
}

export function ScrollComponent(props) {
    const [store, setStore] = useStore()
    pageControls = useAnimation()

    const onScroll = info => {
        data.isPastLimit = info.point.y < -530
        if (data.isPastLimit !== store.isPastLimit) {
            setStore({ isPastLimit: data.isPastLimit })
        }
    }
    return (
        <Scroll
            contentOffsetY={props.scrollProgress * -1}
            width={"100%"}
            height={"100%"}
            onScroll={onScroll}
            scrollAnimate={pageControls}
        >
            {props.children}
        </Scroll>
    )
}

ScrollComponent.defaultProps = {
    ...(Scroll as any).defaultProps,
}

addPropertyControls(ScrollComponent, {
    ...(Scroll as any).propertyControls,
    children: {
        type: ControlType.ComponentInstance,
    },
    scrollProgress: {
        type: ControlType.Number,
        min: 0,
        max: 2000,
    },
})
