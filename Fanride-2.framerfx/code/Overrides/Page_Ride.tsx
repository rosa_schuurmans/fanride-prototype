import * as React from "react"
import { Override, Data } from "framer"

// @ts-ignore
import { getRideById, getProfileById, rides } from "../Content"
// @ts-ignore
import { getAppData, setAppData } from "../State"
// @ts-ignore
import { scrollToTopFunc } from "../scroll/title_scroll"

interface data {
    items: any
    ride: any
    profile: any
    timelineItems: any
    timeline: any
}

const data = Data({
    items: {},
    ride: {},
    timelineItems: {},
    profile: {},
    timeline: [],
    badgesDOM: [],
    badges: [],
})

export function getRidePageData(props): Override {
    //   @ts-ignore
    data.ride = getRideById(getAppData("openRide"))
    // @ts-ignore
    data.profile = getProfileById(data.ride.userId)
    React.useEffect(() => {
        scrollToTopFunc()
    }, [])
    return {
        ...data.ride,
        ...data.profile,
        // @ts-ignore
        user: data.profile.user,
        // @ts-ignore
        carTitle: `${data.profile.user}'s car`,
        text: "Your ride to Lowlands Festival",
    }
}

export function getCurrentPageBadges(props, state): Override {
    data.badgesDOM[props.id] = props.id

    const index = Object.keys(data.badgesDOM).indexOf(props.id)
    const userId = getRideById(getAppData("openRide")).userId
    //   @ts-ignore
    data.badges = getProfileById(userId).badges
    const badge = data.badges[index]

    return {
        socialBadge: badge,
    }
}

export function ClickOnDriver(props, state): Override {
    return {
        onTap() {
            // @ts-ignore
            setAppData("openProfile", data.profile.id)
            scrollToTopFunc()
        },
    }
}

export function timelineItem(props, state): Override {
    data.timelineItems[props.id] = props.id

    const index = Object.keys(data.timelineItems).indexOf(props.id)
    //   @ts-ignore
    data.timeline = getRideById(getAppData("openRide")).timeline
    // @ts-ignore
    const timelineItem = data.timeline[index]

    return {
        ...timelineItem,
    }
}
