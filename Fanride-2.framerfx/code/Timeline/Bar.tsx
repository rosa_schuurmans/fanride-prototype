import * as React from "react"
import { Frame, addPropertyControls, ControlType } from "framer"

// Open Preview (CMD + P)
// API Reference: https://www.framer.com/api

export function Bar(props) {
    const outerStyle = {
        width: "2px",
        height: "100%",
        backgroundColor: props.inActiveColor,
    }

    const innerStyle = {
        width: "2px",
        height: `${props.barProgress}%`,
        backgroundColor: props.activeColor,
    }

    return (
        <Frame style={outerStyle}>
            <Frame style={innerStyle}></Frame>
        </Frame>
    )
}

Bar.defaultProps = {
    activeColor: "rgb(32, 79, 139)",
    inActiveColor: "rgb(189, 223, 73)",
}

addPropertyControls(Bar, {
    activeColor: {
        type: ControlType.Color,
        title: "activeColor",
        defaultValue: "#fff",
    },
    inActiveColor: {
        type: ControlType.Color,
        title: "inActiveColor",
        defaultValue: "#fff",
    },
    barProgress: {
        type: ControlType.Number,
        title: "Bar Progress",
        defaultValue: 50,
        min: 0,
        max: 100,
        unit: "%",
    },
})
